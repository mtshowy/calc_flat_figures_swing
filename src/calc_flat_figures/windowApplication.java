package calc_flat_figures;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.JSeparator;
import javax.swing.JEditorPane;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.DropMode;
import javax.swing.UIManager;

public class windowApplication {

	private JFrame window;
	private JLayeredPane layeredPane;
	private JButton btnSquare, btnRectangle, btnDiamond, btnTriangle, btnCircle;
	private JPanel panelSquare, panelRectangle, panelDiamond, panelTriangle, panelCircle;
	private JTextField squareInput;
	private JButton btnSCalc;
	private JLabel lblSFUnit;
	private JLabel lblSCUnit;
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					windowApplication app = new windowApplication();
					app.window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void switchPanels(JPanel panel) {
		layeredPane.removeAll();
		layeredPane.add(panel);
		layeredPane.repaint();
		layeredPane.revalidate();
	}

	private void initializeWindow() {
		window = new JFrame("Kalkulator figur p�askich");
		window.getContentPane().setForeground(new Color(51, 51, 51));
		window.getContentPane().setBackground(Color.decode("#313131"));
		window.setSize(580, 400);
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.getContentPane().setLayout(null);
	}
	
	private void buttons() {
		btnSquare = new JButton("Kwadrat");
		btnSquare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switchPanels(panelSquare);
			}
		});
		btnSquare.setBounds(12, 12, 98, 26);
		
		btnRectangle = new JButton("Prostok�t");
		btnRectangle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchPanels(panelRectangle);
			}
		});
		btnRectangle.setBounds(122, 12, 98, 26);
		
		btnDiamond = new JButton("Romb");
		btnDiamond.setBounds(232, 12, 98, 26);
		
		btnTriangle = new JButton("Tr�jk�t");
		btnTriangle.setBounds(342, 12, 98, 26);
		
		btnCircle = new JButton("Ko�o");
		btnCircle.setBounds(452, 12, 98, 26);
		
		window.getContentPane().add(btnSquare);
		window.getContentPane().add(btnRectangle);
		window.getContentPane().add(btnDiamond);
		window.getContentPane().add(btnTriangle);
		window.getContentPane().add(btnCircle);
	}
	
	private void drawPanels() {
		layeredPane = new JLayeredPane();
		layeredPane.setBackground(Color.decode("#3E3E3E"));
		layeredPane.setBounds(12, 50, 540, 299);
		window.getContentPane().add(layeredPane);
		layeredPane.setLayout(new CardLayout(0, 0));
		
		panelRectangle = new JPanel();
		panelRectangle.setBackground(Color.decode("#3E3E3E"));
		panelDiamond = new JPanel();
		panelTriangle = new JPanel();
		panelCircle = new JPanel();
		
		
		layeredPane.add(panelRectangle);
		layeredPane.add(panelDiamond);
		layeredPane.add(panelTriangle);
		layeredPane.add(panelCircle);
	}
	
	private void squarePanel() {
		
		panelSquare = new JPanel();
		panelSquare.setBackground(Color.decode("#3E3E3E"));
		layeredPane.add(panelSquare);
		panelSquare.setLayout(null);
		
		JLabel lblSTitle = new JLabel("Kwadrat");
		lblSTitle.setForeground(Color.WHITE);
		lblSTitle.setFont(new Font("Bahnschrift", Font.PLAIN, 42));
		lblSTitle.setBounds(186, 5, 158, 51);
		panelSquare.add(lblSTitle);
		
		JLabel lblSInput = new JLabel("Podaj bok a:");
		lblSInput.setForeground(Color.WHITE);
		lblSInput.setFont(new Font("Dialog", Font.BOLD, 18));
		lblSInput.setBounds(370, 76, 110, 26);
		panelSquare.add(lblSInput);
		
		squareInput = new JTextField();
		squareInput.setFont(new Font("Dialog", Font.BOLD, 14));
		squareInput.setForeground(Color.WHITE);
		squareInput.setHorizontalAlignment(SwingConstants.CENTER);
		squareInput.setBackground(Color.decode("#606060"));
		squareInput.setBounds(364, 111, 120, 24);
		panelSquare.add(squareInput);
		squareInput.setColumns(1);
		
		JLabel lblSField = new JLabel("Pole wynosi:");
		lblSField.setBackground(new Color(153, 153, 153));
		lblSField.setForeground(Color.WHITE);
		lblSField.setFont(new Font("Dialog", Font.BOLD, 14));
		lblSField.setBounds(255, 202, 97, 21);
		panelSquare.add(lblSField);
		
		JLabel lblCField = new JLabel("Obw�d wynosi:");
		lblCField.setForeground(Color.WHITE);
		lblCField.setFont(new Font("Dialog", Font.BOLD, 14));
		lblCField.setBounds(255, 236, 110, 21);
		panelSquare.add(lblCField);
		
		JTextArea areaSFResult = new JTextArea();
		areaSFResult.setTabSize(1);
		areaSFResult.setFont(new Font("Dialog", Font.BOLD, 14));
		areaSFResult.setForeground(Color.WHITE);
		areaSFResult.setBackground(Color.decode("#606060"));
		areaSFResult.setEditable(false);
		areaSFResult.setBounds(373, 202, 104, 22);
		panelSquare.add(areaSFResult);
		
		JTextArea areaSCResult = new JTextArea();
		areaSCResult.setColumns(1);
		areaSCResult.setTabSize(1);
		areaSCResult.setFont(new Font("Dialog", Font.BOLD, 14));
		areaSCResult.setForeground(Color.WHITE);
		areaSCResult.setBackground(Color.decode("#606060"));
		areaSCResult.setEditable(false);
		areaSCResult.setBounds(373, 236, 104, 22);
		panelSquare.add(areaSCResult);
		
		btnSCalc = new JButton("Oblicz");
		btnSCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sFResult, sCResult;
				double sInputValue, sFCalc, sCCalc;
				
				if (isDouble(squareInput.getText())) {
					sInputValue = Double.parseDouble(squareInput.getText());
				}	else {
						Object[] options = { "OK"};
						JOptionPane.showOptionDialog(null, "Podaj liczb�, nie tekst!", "Uwaga!",
						JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
						
						squareInput.grabFocus();
						return;
					}
				
				sFCalc = sInputValue * sInputValue;
				sCCalc = 4 * sInputValue;
				
				sFResult = String.format("%.2f", sFCalc);
				sCResult = String.format("%.2f", sCCalc);
				
				areaSFResult.setText("   "+sFResult);
				areaSCResult.setText("   "+sCResult);
			}
		});
		
		btnSCalc.setBounds(364, 146, 120, 26);
		panelSquare.add(btnSCalc);
		
		lblSFUnit = new JLabel("cm2");
		lblSFUnit.setForeground(Color.WHITE);
		lblSFUnit.setFont(new Font("Dialog", Font.BOLD, 14));
		lblSFUnit.setBackground(UIManager.getColor("Button.disabledText"));
		lblSFUnit.setBounds(488, 202, 40, 21);
		panelSquare.add(lblSFUnit);
		
		lblSCUnit = new JLabel("cm");
		lblSCUnit.setForeground(Color.WHITE);
		lblSCUnit.setFont(new Font("Dialog", Font.BOLD, 14));
		lblSCUnit.setBackground(UIManager.getColor("Button.disabledText"));
		lblSCUnit.setBounds(488, 236, 40, 21);
		panelSquare.add(lblSCUnit);
	}
	
	public windowApplication() {
		initializeWindow();
		buttons();
		drawPanels();
		squarePanel();
	}
	
	private static boolean isDouble(String m) {
		try {
			Double.parseDouble(m);
			return true;
		}
		catch (NumberFormatException e) {
			return false;
		}
	}
}


